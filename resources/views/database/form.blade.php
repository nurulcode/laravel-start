<div class="modal fade" id="form-modal">
    <div class="modal-dialog  modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title"></h4>
            </div>
            <div class="modal-body">
                <form class="form-horizontal" id="form" enctype="multipart/form-data">

                    <input type="hidden" id="id" name="id">

                    <div class="form-group">
                        <label for="nama" class="col-sm-2 control-label text-uppercase">Logo : </label>
                        <div class="col-sm-10">
                            <input type="file" class="form-control" name="logo" id="logo">
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="nama" class="col-sm-2 control-label text-uppercase">nama : </label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" id="nama" name="nama" required
                                placeholder="Input Here">
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="email" class="col-sm-2 control-label text-uppercase">E Mail : </label>
                        <div class="col-sm-10">
                            <input type="email" class="form-control" id="email" name="email" required
                                placeholder="Input Here">
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="kabupaten" class="col-sm-2 control-label text-uppercase">Kabupaten : </label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" id="kabupaten" name="kabupaten" required
                                placeholder="Input Here">
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="alamat" class="col-sm-2 control-label text-uppercase">Alamat : </label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" id="alamat" name="alamat" required
                                placeholder="Input Here">
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="telepon" class="col-sm-2 control-label text-uppercase">telepon : </label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" id="telepon" name="telepon" required
                                placeholder="Input Here">
                        </div>
                    </div>
                </form>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-primary" id="submit">Save</button>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
