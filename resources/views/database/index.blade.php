@extends('layouts.global')
@section('title') Setting Manajemen @endsection
@section('content')
    <div class="box">
        <div class="box-header">
            <div class="row">
                <div class="col-md-6">
                    <h3 class="box-title">Data Table Setting</h3>
                </div>
                <div class="col-md-6 text-right">
                    <button type="button" class="btn btn-primary" id="tambahData">Tambah Data</button>
                </div>
            </div>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
            <table id="table" class="table table-bordered table-striped">
                <thead>
                    <tr>
                        <th>Nama</th>
                        <th>Email</th>
                        <th>Kabupaten</th>
                        <th>Alamat</th>
                        <th>telepon</th>
                        <th style="width: 100px">Action</th>
                    </tr>
                </thead>
            </table>
        </div>
        <!-- /.box-body -->
    </div>
    <div class="row">
        @include('database.form')
    </div>
@endsection

@section('javascript')
    <script type="application/javascript">
        $('#datepicker2').datepicker({
            todayBtn: 'linked',
            format: 'yyyy-mm-dd',
            autoclose: true
        });

        $(document).ready(function() {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            let table = $('#table').DataTable({
                processing: true,
                serverSide: true,
                ajax: {
                    url: "{{ route('database.index') }}",
                    type: 'GET'
                },
                columnDefs: [{
                    orderable: true,
                    className: 'text-center',
                    targets: [5]
                }],
                columns: [{
                    data: 'nama',
                }, {
                    data: 'email',
                }, {
                    data: 'kabupaten'
                }, {
                    data: 'alamat'
                }, {
                    data: 'telepon'
                }, {
                    data: 'action',
                    orderable: false,
                    searchable: false,
                }],
                order: [
                    [0, 'asc']
                ]
            });
        });

        // show modal create
        $('#tambahData').click(function() {
            $('.modal-title').html('Tambah Data');
            $('#id').val('');
            $('#form').trigger("reset");
            $('#form-modal').modal('show');
        });

        // show modal edit
        $('body').on('click', '.btnEdit', function() {
            $('.modal-title').html('Edit Data');
            let id = $(this).data('id');
            $.get('database/' + id + '/edit', function(data) {
                $('#form-modal').modal('show');
                $('#id').val(data.database.id);
                $('#nama').val(data.database.nama);
                $('#kabupaten').val(data.database.kabupaten);
                $('#agama').val(data.database.agama);
                $('#telepon').val(data.database.telepon);
                $('#alamat').val(data.database.alamat);
                $('#email').val(data.database.email);
            })
        });

        // post, put
        $('#submit').click(function(e) {
            e.preventDefault();
            $('#submit').html('Loading...');
            $.ajax({
                processData: false,
                contentType: false,
                data: new FormData(document.getElementById("form")),
                url: "{{ route('database.store') }}",
                type: "POST",
                dataType: 'json',
                success: function(data) {
                    toast(data)
                    $('#form').trigger("reset");
                    $('#submit').html('Save');
                    $('#form-modal').modal('hide');
                    let oTable = $('#table').dataTable();
                    oTable.fnDraw(false);
                },
                error: function(data) {
                    $('#submit').html('Save');
                    console.log('Error:', data);
                    toast(data)
                }
            });
        });

        // delete
        $(document).on('click', '.btnDelete', function() {
            if (confirm("Ingin menghapus data, data yg terhapus tidak bisa di kambalikan!")) {

                let id = $(this).attr('id');
                $.ajax({
                    url: "/database/" + id,
                    type: 'delete',
                    success: function(data) {
                        toast(data);
                        setTimeout(function() {
                            let oTable = $('#table').dataTable();
                            oTable.fnDraw(false);
                        });
                    }
                })
            }
        });
    </script>
@endsection
