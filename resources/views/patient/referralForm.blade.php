<div class="modal fade" id="form-modal-referral">
    <div class="modal-dialog  modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title-referral"></h4>
            </div>
            <div class="modal-body">
                <form class="form-horizontal" id="form-referral">

                    <input type="hidden" id="patient_id" name="patient_id">

                    <div class="form-group">
                        <label for="name" class="col-sm-2 control-label text-uppercase">No RM : </label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" required placeholder="Terisi Otomatis"
                                id='no_rekam_medis_pasien' readonly>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="catatan" class="col-sm-2 control-label text-uppercase">Catatan : </label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" required id='catatan' name="catatan" placeholder="Catatan Pemeriksaan">
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="tgl_rujukan" class="col-sm-2 control-label text-uppercase">Tgl Sample : </label>
                        <div class="col-sm-10">
                            <div class="input-group date">
                                <div class="input-group-addon">
                                    <i class="fa fa-calendar"></i>
                                </div>
                                <input type="text" class="form-control pull-right" id="datepicker3" name="tgl_rujukan"
                                    autocomplete="off">
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="analis" class="col-sm-2 control-label text-uppercase">Analis : </label>
                        <div class="col-sm-10">
                            <select class="form-control text-uppercase select2" style="width: 100%;" id="analis" name="analis">
                                <option value="">--Pilih--</option>
                                @foreach ($employees as $key => $v)
                                @if ($v->kategori == 'Analis')
                                    <option value="{{ $v->id }}">{{ $v->nama }}</option>
                                @endif
                                @endforeach
                            </select>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="dokter" class="col-sm-2 control-label text-uppercase">Dokter :
                        </label>
                        <div class="col-sm-10">
                            <select class="form-control text-uppercase select2" style="height: 100%; width: 100%;" id="dokter" name="dokter">
                                <option value="">--Pilih--</option>
                                @foreach ($employees as $key => $v)
                                @if ($v->kategori == 'Dokter')
                                    <option value="{{ $v->id }}">{{ $v->nama }}</option>
                                @endif
                                @endforeach
                            </select>
                        </div>
                    </div>
                </form>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-primary" id="submit-referral">Save</button>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
</div>
