@extends('layouts.global')
@section('title') Pasien Manajemen @endsection
@section('content')
    <div class="box">
        <div class="box-header">
            <div class="row">
                <div class="col-md-6">
                    <h3 class="box-title">Data Table Pasien</h3>
                </div>
                <div class="col-md-6 text-right">
                    <button type="button" class="btn btn-primary" id="tambahData">Tambah Data</button>
                </div>
            </div>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
            <table id="table" class="table table-bordered table-striped">
                <thead>
                    <tr>
                        <th>RM</th>
                        <th>NIK</th>
                        <th>Nama</th>
                        <th>Alamat</th>
                        <th style="width: 100px">Action</th>
                    </tr>
                </thead>
            </table>
        </div>
        <!-- /.box-body -->
    </div>
    <div class="row">
        @include('patient.referralForm')
        @include('patient.form')
    </div>
@endsection

@section('javascript')
    <script type="application/javascript">
        $('.select2').select2()


        $('#datepicker2').datepicker({
            todayBtn: 'linked',
            format: 'yyyy-mm-dd',
            autoclose: true
        });

        $('#datepicker3').datepicker({
            todayBtn: 'linked',
            format: 'yyyy-mm-dd',
            autoclose: true
        });

        $(document).ready(function() {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            let table = $('#table').DataTable({
                processing: true,
                serverSide: true,
                ajax: {
                    url: "{{ route('patient.index') }}",
                    type: 'GET'
                },
                columnDefs: [{
                    orderable: true,
                    className: 'text-center',
                    targets: [4]
                }],
                columns: [{
                    data: 'no_rekam_medis',
                }, {
                    data: 'nik',
                }, {
                    data: 'nama'
                }, {
                    data: 'alamat'
                }, {
                    data: 'action',
                    orderable: false,
                    searchable: false,
                }],
                order: [
                    [0, 'asc']
                ]
            });
        });

        // show modal create
        $('#tambahData').click(function() {
            $('.modal-title').html('Tambah Data');
            $('#id').val('');
            $('#form').trigger("reset");
            $('#form-modal').modal('show');
        });

        // show modal edit
        $('body').on('click', '.btnEdit', function() {
            $('.modal-title').html('Edit Data');
            let id = $(this).data('id');
            $.get('patient/' + id + '/edit', function(data) {
                $('#form-modal').modal('show');
                $('#id').val(data.patient.id);
                $('#no_rekam_medis').val(data.patient.no_rekam_medis);
                $('#nama').val(data.patient.nama);
                $('#nik').val(data.patient.nik);
                $('#tempat_lahir').val(data.patient.tempat_lahir);
                $('#datepicker2').datepicker('setDate', data.patient.tgl_lahir);
                $('#agama').val(data.patient.agama);
                $('#telepon').val(data.patient.telepon);
                $('#alamat').val(data.patient.alamat);
                $('#jenis_kelamin').val(data.patient.jenis_kelamin);
                $('#pekerjaan').val(data.patient.pekerjaan);
                $('#golongan_darah').val(data.patient.golongan_darah);
                $('#tinggi_badan').val(data.patient.tinggi_badan);
                $('#berat_badan').val(data.patient.berat_badan);
            })
        });

        // post, put
        $('#submit').click(function(e) {
            e.preventDefault();
            $('#submit').html('Loading...');
            $.ajax({
                data: $('#form').serialize(),
                url: "{{ route('patient.store') }}",
                type: "POST",
                dataType: 'json',
                success: function(data) {
                    toast(data)
                    $('#form').trigger("reset");
                    $('#submit').html('Save');
                    $('#form-modal').modal('hide');
                    let oTable = $('#table').dataTable();
                    oTable.fnDraw(false);
                },
                error: function(data) {
                    $('#submit').html('Save');
                    console.log('Error:', data);
                    toast(data)
                }
            });
        });

        // delete
        $(document).on('click', '.btnDelete', function() {
            if (confirm("Ingin menghapus data, data yg terhapus tidak bisa di kambalikan!")) {

                let id = $(this).attr('id');
                $.ajax({
                    url: "/patient/" + id,
                    type: 'delete',
                    success: function(data) {
                        toast(data);
                        setTimeout(function() {
                            let oTable = $('#table').dataTable();
                            oTable.fnDraw(false);
                        });
                    }
                })
            }
        });


        // show modal referral
        $('body').on('click', '.btnRujuk', function() {
            $('.modal-title').html('Edit Data');
            let id = $(this).data('id');
            let no_rekam_medis = $(this).data('no_rekam_medis');
            $('#patient_id').val(id);
            $('#no_rekam_medis_pasien').val(no_rekam_medis);
            $('.modal-title-referral').html('Rujukan Pemeriksaan');
            $('#form-modal-referral').modal('show');
        });

        // post, put referral
        $('#submit-referral').click(function(e) {
            e.preventDefault();
            $('#submit-referral').html('Loading...');
            $.ajax({
                data: $('#form-referral').serialize(),
                url: "{{ route('patient-referral.store') }}",
                type: "POST",
                dataType: 'json',
                success: function(data) {
                    toast(data)
                    $('#form-referral').trigger("reset");
                    $('#submit-referral').html('Save');
                    $('#form-modal-referral').modal('hide');
                    let oTable = $('#table').dataTable();
                    oTable.fnDraw(false);
                },
                error: function(data) {
                    $('#submit-referral').html('Save');
                    console.log('Error:', data);
                    toast(data)
                }
            });
        });
    </script>
@endsection
