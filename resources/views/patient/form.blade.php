<div class="modal fade" id="form-modal">
    <div class="modal-dialog  modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title"></h4>
            </div>
            <div class="modal-body">
                <form class="form-horizontal" id="form">

                    <input type="hidden" id="id" name="id">

                    <div class="form-group" style="margin: 5px">
                        <label for="name" class="col-sm-2 control-label text-uppercase">No RM : </label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" required placeholder="Terisi Otomatis" id='no_rekam_medis' readonly>
                        </div>
                    </div>
                    <div class="form-group" style="margin: 5px">
                        <label for="nik" class="col-sm-2 control-label text-uppercase">Nik : </label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" id="nik" name="nik" required
                                placeholder="Input Here">
                        </div>
                    </div>
                    <div class="form-group" style="margin: 5px">
                        <label for="nama" class="col-sm-2 control-label text-uppercase">nama : </label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" id="nama" name="nama" required
                                placeholder="Input Here">
                        </div>
                    </div>
                    <div class="form-group" style="margin: 5px">
                        <label for="tempat_lahir" class="col-sm-2 control-label text-uppercase">tempat lahir : </label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" id="tempat_lahir" name="tempat_lahir" required
                                placeholder="Input Here">
                        </div>
                    </div>
                    <div class="form-group" style="margin: 5px">
                        <label for="tgl_lahir" class="col-sm-2 control-label text-uppercase">tgl lahir : </label>
                        <div class="col-sm-10">
                            <div class="input-group date">
                                <div class="input-group-addon">
                                    <i class="fa fa-calendar"></i>
                                </div>
                                <input type="text" class="form-control pull-right" id="datepicker2" name="tgl_lahir" autocomplete="off">
                            </div>
                        </div>
                    </div>
                    <div class="form-group" style="margin: 5px">
                        <label for="agama" class="col-sm-2 control-label text-uppercase">agama : </label>
                        <div class="col-sm-10">
                            <select class="form-control text-uppercase" id="agama" name="agama">
                                <option value="">--Pilih--</option>
                                @foreach(App\Enums\AgamaEnum::asSelectArray() as $key => $v)
                                <option value="{{ $v }}">{{ $v }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="form-group" style="margin: 5px">
                        <label for="jenis_kelamin" class="col-sm-2 control-label text-uppercase">jenis kelamin : </label>
                        <div class="col-sm-10">
                            <select class="form-control text-uppercase" id="jenis_kelamin" name="jenis_kelamin">
                                <option value="">--Pilih--</option>
                                @foreach(App\Enums\JenisKelaminEnum::asSelectArray() as $key => $v)
                                <option value="{{ $v }}">{{ $v }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="form-group" style="margin: 5px">
                        <label for="pekerjaan" class="col-sm-2 control-label text-uppercase">pekerjaan : </label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" id="pekerjaan" name="pekerjaan" required
                                placeholder="Input Here">
                        </div>
                    </div>
                    <div class="form-group" style="margin: 5px">
                        <label for="telepon" class="col-sm-2 control-label text-uppercase">telepon : </label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" id="telepon" name="telepon" required
                                placeholder="Input Here">
                        </div>
                    </div>
                    <div class="form-group" style="margin: 5px">
                        <label for="alamat" class="col-sm-2 control-label text-uppercase">alamat : </label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" id="alamat" name="alamat" required
                                placeholder="Input Here">
                        </div>
                    </div>
                    {{-- <hr> --}}
                    {{-- <div class="form-group" style="margin: 5px">
                        <label for="password" class="col-sm-2 control-label text-uppercase">Gol Da : </label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" id="golongan_darah" name="golongan_darah" required
                                placeholder="Input Here">
                        </div>
                    </div>
                    <div class="form-group" style="margin: 5px">
                        <label for="password" class="col-sm-2 control-label text-uppercase">tinggi badan : </label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" id="tinggi_badan" name="tinggi_badan" required
                                placeholder="Input Here">
                        </div>
                    </div>
                    <div class="form-group" style="margin: 5px">
                        <label for="password" class="col-sm-2 control-label text-uppercase">berat badan : </label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" id="berat_badan" name="berat_badan" required
                                placeholder="Input Here">
                        </div>
                    </div> --}}
                </form>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-primary" id="submit">Save</button>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
