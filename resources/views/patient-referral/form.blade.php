<div class="modal fade" id="form-modal">
    <div class="modal-dialog  modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title"></h4>
            </div>
            <div class="modal-body">
                <form class="form-horizontal" id="form">

                    <input type="hidden" id="patient_referral_id" name="patient_referral_id">

                    <div class="form-group">
                        <label for="inspection_id" class="col-sm-2 control-label text-uppercase">Nama Pemeriksaan :
                        </label>
                        <div class="col-sm-10">
                            <select class="form-control text-uppercase select2" style="width: 100%;" id="inspection_id"
                                name="inspection_id">
                                <option value="">--Pilih--</option>
                                @foreach ($inspections as $key => $v)
                                    <option value="{{ $v->id }}">{{ $v->uraian }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="tgl_pemeriksaan" class="col-sm-2 control-label text-uppercase">tgl pemeriksaan :
                        </label>
                        <div class="col-sm-10">
                            <div class="input-group date">
                                <div class="input-group-addon">
                                    <i class="fa fa-calendar"></i>
                                </div>
                                <input type="text" class="form-control pull-right" id="datepicker3"
                                    name="tgl_pemeriksaan" autocomplete="off">
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="pukul_pemeriksaan" class="col-sm-2 control-label text-uppercase">pukul pemeriksaan :
                        </label>
                        <div class="col-sm-10">
                            <input type="time" class="form-control" id="pukul_pemeriksaan" name="pukul_pemeriksaan"
                                required placeholder="Input Here">
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="hasil" class="col-sm-2 control-label text-uppercase">Hasil : </label>
                        <div class="col-sm-10">
                            <select class="form-control text-uppercase" id="hasil" name="hasil">
                                <option value="">--Pilih--</option>
                                @foreach(App\Enums\HasilEnum::asSelectArray() as $key => $v)
                                <option value="{{ $v }}">{{ $v }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="catatan" class="col-sm-2 control-label text-uppercase">catatan : </label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" id="catatan" name="catatan" required
                                placeholder="Input Here">
                        </div>
                    </div>
                </form>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-primary" id="submit">Save</button>
                </div>

                <table id="table-referral" class="table table-bordered table-striped" style="width: 100%">
                    <thead>
                        <tr>
                            <th>Pemeriksaan</th>
                            <th>Tgl Pemeriksaan</th>
                            <th>Hasil</th>
                            <th>Catatan</th>
                            <th style="width: 100px">Action</th>
                        </tr>
                    </thead>
                </table>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
</div>
