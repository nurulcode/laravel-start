@extends('layouts.global')
@section('title') Patient Referral Manajemen @endsection
@section('content')
    <div class="box">
        <div class="box-header">
            <div class="row">
                <div class="col-md-12">
                    <h3 class="box-title">Data Table Patient Referral</h3>
                </div>
                {{-- <div class="col-md-6 text-right">
                    <button type="button" class="btn btn-primary" id="tambahData">Tambah Data</button>
                </div> --}}
            </div>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
            <table id="table" class="table table-bordered table-striped">
                <thead>
                    <tr>
                        <th>NO</th>
                        <th>RM</th>
                        <th>Nama Pasien</th>
                        <th>Tgl Pemeriksaan</th>
                        <th>Catatan</th>
                        <th>Analis</th>
                        <th>Dokter</th>
                        <th style="width: 100px">Action</th>
                    </tr>
                </thead>
            </table>
        </div>
        <!-- /.box-body -->
    </div>
    <div class="row">
        @include('patient-referral.form')
    </div>
@endsection

@section('javascript')
    <script type="application/javascript">
        $('#datepicker3').datepicker({
            todayBtn: 'linked',
            format: 'yyyy-mm-dd',
            autoclose: true
        });

        $(document).ready(function() {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            let table = $('#table').DataTable({
                processing: true,
                serverSide: true,
                ajax: {
                    url: "{{ route('patient-referral.index') }}",
                    type: 'GET'
                },
                columnDefs: [{
                    orderable: true,
                    className: 'text-center',
                    targets: [7]
                }],
                columns: [{
                    name: 'patients.id',
                    data: 'id',
                }, {
                    name: 'patients.no_rekam_medis',
                    data: 'no_rekam_medis',
                }, {
                    name: 'patients.nama',
                    data: 'nama',
                }, {
                    name: 'patient_referrals.tgl_rujukan',
                    data: 'tgl_rujukan'
                }, {
                    name: 'patient_referrals.catatan',
                    data: 'catatan',
                }, {
                    name: 'analis.nama',
                    data: 'analis',
                }, {
                    name: 'dokter.nama',
                    data: 'dokter',
                }, {
                    data: 'action',
                    orderable: false,
                    searchable: false,
                }],
                order: [
                    [0, 'asc']
                ]
            });
        });

        // show modal create
        $('#tambahData').click(function() {
            $('.modal-title').html('Tambah Data');
            $('#id').val('');
            $('#form').trigger("reset");
            $('#form-modal').modal('show');
        });

        // show modal edit
        $('body').on('click', '.btnHasil', function() {
            $('.modal-title').html('Form Pemeriksaan');
            let id = $(this).data('id');
            $('#patient_referral_id').val(id);
            $('#form-modal').modal('show');

            let table = $('#table-referral').DataTable({
                processing: true,
                bDestroy: true,
                serverSide: true,
                ajax: {
                    url: "{{ route('patient-examination.index') }}",
                    data: {
                        id: id,
                    },
                    type: 'GET'
                },
                columnDefs: [{
                    orderable: true,
                    className: 'text-center',
                    targets: [4]
                }],
                columns: [{
                    name: 'inspections.uraian',
                    data: 'uraian',
                }, {
                    name: 'tgl_pemeriksaan',
                    data: 'tgl_pemeriksaan',
                }, {
                    name: 'hasil',
                    data: 'hasil',
                }, {
                    name: 'catatan',
                    data: 'catatan',
                }, {
                    data: 'action',
                    orderable: false,
                    searchable: false,
                }],
                order: [
                    [0, 'asc']
                ]
            });

        });

        // post, put
        $('#submit').click(function(e) {
            e.preventDefault();
            $('#submit').html('Loading...');
            $.ajax({
                data: $('#form').serialize(),
                url: "{{ route('patient-examination.store') }}",
                type: "POST",
                dataType: 'json',
                success: function(data) {
                    toast(data)
                    $('#form').trigger("reset");
                    $('#submit').html('Save');
                    let oTable = $('#table-referral').dataTable();
                    oTable.fnDraw(false);
                    let tTable = $('#table').dataTable();
                    tTable.fnDraw(false);
                },
                error: function(data) {
                    $('#submit').html('Save');
                    console.log('Error:', data);
                    toast(data)
                }
            });
        });

        // delete
        $(document).on('click', '.btnDelete', function() {
            if (confirm("Ingin menghapus data, data yg terhapus tidak bisa di kambalikan!")) {

                let id = $(this).data('id');
                $.ajax({
                    url: "/patient-examination/" + id,
                    type: 'delete',
                    success: function(data) {
                        toast(data);
                        setTimeout(function() {
                            let oTable = $('#table-referral').dataTable();
                            oTable.fnDraw(false);
                            let tTable = $('#table').dataTable();
                            tTable.fnDraw(false);
                        });
                    }
                })
            }
        });
    </script>
@endsection
