@extends('layouts.global')
@section('title') Employee Manajemen @endsection
@section('content')
    <div class="box">
        <div class="box-header">
            <div class="row">
                <div class="col-md-6">
                    <h3 class="box-title">Data Table Employee</h3>
                </div>
                <div class="col-md-6 text-right">
                    <button type="button" class="btn btn-primary" id="tambahData">Tambah Data</button>
                </div>
            </div>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
            <table id="table" class="table table-bordered table-striped">
                <thead>
                    <tr>
                        <th>Nama</th>
                        <th>Tempat Lahir</th>
                        <th>Tgl Lahir</th>
                        <th>Telepon</th>
                        <th>Alamat</th>
                        <th>Kategori</th>
                        <th style="width: 100px">Action</th>
                    </tr>
                </thead>
            </table>
        </div>
        <!-- /.box-body -->
    </div>
    <div class="row">
        @include('employee.form')
    </div>
@endsection

@section('javascript')
    <script type="application/javascript">
        $('#datepicker2').datepicker({
            todayBtn: 'linked',
            format: 'yyyy-mm-dd',
            autoclose: true
        });

        $(document).ready(function() {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            let table = $('#table').DataTable({
                processing: true,
                serverSide: true,
                ajax: {
                    url: "{{ route('employee.index') }}",
                    type: 'GET'
                },
                columnDefs: [{
                    orderable: true,
                    className: 'text-center',
                    targets: [6]
                }],
                columns: [{
                    data: 'nama',
                }, {
                    data: 'tempat_lahir',
                }, {
                    data: 'tgl_lahir'
                }, {
                    data: 'telepon'
                }, {
                    data: 'alamat'
                }, {
                    data: 'kategori'
                }, {
                    data: 'action',
                    orderable: false,
                    searchable: false,
                }],
                order: [
                    [0, 'asc']
                ]
            });
        });

        // show modal create
        $('#tambahData').click(function() {
            $('.modal-title').html('Tambah Data');
            $('#id').val('');
            $('#form').trigger("reset");
            $('#form-modal').modal('show');
        });

        // show modal edit
        $('body').on('click', '.btnEdit', function() {
            $('.modal-title').html('Edit Data');
            let id = $(this).data('id');
            $.get('employee/' + id + '/edit', function(data) {
            $('#form-modal').modal('show');
                $('#id').val(data.employee.id);
                $('#nama').val(data.employee.nama);
                $('#tempat_lahir').val(data.employee.tempat_lahir);
                $('#datepicker2').datepicker('setDate', data.employee.tgl_lahir);
                $('#jenis_kelamin').val(data.employee.jenis_kelamin);
                $('#agama').val(data.employee.agama);
                $('#telepon').val(data.employee.telepon);
                $('#alamat').val(data.employee.alamat);
                $('#kategori').val(data.employee.kategori);
            })
        });

        // post, put
        $('#submit').click(function(e) {
            e.preventDefault();
            $('#submit').html('Loading...');
            $.ajax({
                data: $('#form').serialize(),
                url: "{{ route('employee.store') }}",
                type: "POST",
                dataType: 'json',
                success: function(data) {
                    toast(data)
                    $('#form').trigger("reset");
                    $('#submit').html('Save');
                    $('#form-modal').modal('hide');
                    let oTable = $('#table').dataTable();
                    oTable.fnDraw(false);
                },
                error: function(data) {
                    $('#submit').html('Save');
                    console.log('Error:', data);
                    toast(data)
                }
            });
        });

        // delete
        $(document).on('click', '.btnDelete', function() {
            if (confirm("Ingin menghapus data, data yg terhapus tidak bisa di kambalikan!")) {

                let id = $(this).attr('id');
                $.ajax({
                    url: "/employee/" + id,
                    type: 'delete',
                    success: function(data) {
                        toast(data);
                        setTimeout(function() {
                            let oTable = $('#table').dataTable();
                            oTable.fnDraw(false);
                        });
                    }
                })
            }
        });
    </script>
@endsection
