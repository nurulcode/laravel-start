<ul class="nav navbar-nav">
    <li>
        <a href="{{ route('home') }}">Home</a>
    </li>

    <li class="dropdown">
        <a href="#" class="dropdown-toggle" data-toggle="dropdown">Register
            <span class="caret"></span>
        </a>
        <ul class="dropdown-menu" role="menu">
            <li><a href="{{ route('patient.index') }}">Pasien Baru </a></li>
        </ul>
    </li>

    <li>
        <a href="{{ route('patient-referral.index') }}">Layanan Pemeriksaan</a>
    </li>

    <li class="dropdown">
        <a href="#" class="dropdown-toggle" data-toggle="dropdown">Master Data
            <span class="caret"></span>
        </a>
        <ul class="dropdown-menu" role="menu">
            <li><a href="{{ route('database.index') }}">Setting</a></li>
            <li><a href="{{ route('inspection.index') }}">Daftar Pemeriksaan</a></li>
            <li><a href="{{ route('employee.index') }}">Daftar Pegawai / Tenaga Medis</a></li>
        </ul>
    </li>
    <li class="dropdown">
        <a href="#" class="dropdown-toggle" data-toggle="dropdown">Laporan
            <span class="caret"></span>
        </a>
        <ul class="dropdown-menu" role="menu">
            <li><a href="{{ route('report-excel.index') }}">Laporan Excel</a></li>
        </ul>
    </li>
    <li class="dropdown">
        <a href="#" class="dropdown-toggle" data-toggle="dropdown">System Support
            <span class="caret"></span>
        </a>
        <ul class="dropdown-menu" role="menu">
            <li><a href="{{ route('user.index') }}">Users</a></li>
        </ul>
    </li>
    {{-- <li class="dropdown">
        <a href="#" class="dropdown-toggle" data-toggle="dropdown">Dropdown
            <span class="caret"></span>
        </a>
        <ul class="dropdown-menu" role="menu">
            <li><a href="#">Action</a></li>
            <li><a href="#">Another action</a></li>
            <li><a href="#">Something else here</a></li>
            <li class="divider"></li>
            <li><a href="#">Separated link</a></li>
            <li class="divider"></li>
            <li><a href="#">One more separated link</a></li>
        </ul>
    </li> --}}
</ul>
