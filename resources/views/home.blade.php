@extends('layouts.global')
@section('title') Home @endsection

@section('content-title') Home @endsection
@section('content')
    <div class="box">
        <div class="box-header">
            <div class="row">
                <div class="col-md-12">
                    <h1 class="box-title">Dashboard Pemeriksaan Tgl {{ $date }}</h1>
                </div>
            </div>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
            <!-- Main content -->
            <section class="content">
                <!-- Small boxes (Stat box) -->
                <div class="row">
                    <div class="col-lg-4 col-xs-6">
                        <!-- small box -->
                        <div class="small-box bg-aqua">
                            <div class="inner">
                                <h3>{{ $pemeriksaan }}</h3>

                                <p>Jumlah Pemeriksaan</p>
                            </div>
                            <div class="icon">
                                <i class="ion ion-pie-graph"></i>
                            </div>
                            <a href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
                        </div>
                    </div>
                    <!-- ./col -->
                    <div class="col-lg-4 col-xs-6">
                        <!-- small box -->
                        <div class="small-box bg-green">
                            <div class="inner">
                                <h3>{{ $negative }}<sup style="font-size: 20px">Pasien</sup></h3>

                                <p>Hasil Negatif / Non Reactive</p>
                            </div>
                            <div class="icon">
                                <i class="ion ion-happy-outline"></i>
                            </div>
                            <a href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
                        </div>
                    </div>
                    <!-- ./col -->
                    <div class="col-lg-4 col-xs-6">
                        <!-- small box -->
                        <div class="small-box bg-red">
                            <div class="inner">
                                <h3>{{ $positive }}<sup style="font-size: 20px">Pasien</sup></h3>

                                <p>Hasil Positif / Reactive</p>
                            </div>
                            <div class="icon">
                                <i class="ion ion-sad-outline"></i>
                            </div>
                            <a href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
                        </div>
                    </div>
                    <!-- ./col -->
                                </div>
                <!-- /.row -->
                <!-- Main row -->
                {{-- <div class="row">
                    <!-- Left col -->
                    <section class="col-lg-7 connectedSortable">
                        <!-- Custom tabs (Charts with tabs)-->
                        <div class="nav-tabs-custom">
                            <!-- Tabs within a box -->
                            <ul class="nav nav-tabs pull-right">
                                <li class="active"><a href="#revenue-chart" data-toggle="tab">Area</a></li>
                                <li><a href="#sales-chart" data-toggle="tab">Donut</a></li>
                                <li class="pull-left header"><i class="fa fa-inbox"></i> Sales</li>
                            </ul>
                            <div class="tab-content no-padding">
                                <!-- Morris chart - Sales -->
                                <div class="chart tab-pane active" id="revenue-chart"
                                    style="position: relative; height: 300px;"></div>
                                <div class="chart tab-pane" id="sales-chart" style="position: relative; height: 300px;">
                                </div>
                            </div>
                        </div>
                        <!-- /.nav-tabs-custom -->
                    </section>
                    <!-- /.Left col -->
                </div> --}}
                <!-- /.row (main row) -->

            </section>
            <!-- /.content -->
        </div>
        <!-- /.box-body -->
    </div>
@endsection
