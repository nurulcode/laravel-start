<div class="modal fade" id="form-modal">
    <div class="modal-dialog  modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title"></h4>
            </div>
            <div class="modal-body">
                <form class="form-horizontal" id="form">

                    <input type="hidden" id="id" name="id">

                    <div class="form-group">
                        <label for="kode" class="col-sm-2 control-label text-uppercase">kode : </label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" id="kode" name="kode" required
                                placeholder="Input Here">
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="uraian" class="col-sm-2 control-label text-uppercase">uraian : </label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" id="uraian" name="uraian" required
                                placeholder="Input Here">
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="satuan" class="col-sm-2 control-label text-uppercase">satuan : </label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" id="satuan" name="satuan" required
                                placeholder="Input Here">
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="nilai_normal" class="col-sm-2 control-label text-uppercase">nilai normal : </label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" id="nilai_normal" name="nilai_normal" required
                                placeholder="Input Here">
                        </div>
                    </div>
                </form>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-primary" id="submit">Save</button>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
