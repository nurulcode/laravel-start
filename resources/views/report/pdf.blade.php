<!DOCTYPE html>
<html>

<head>
    <title>{{ $pasien->nama }}</title>
    <link href="{{ public_path('assets/dist/css/bootstrap.min.css') }}" rel="stylesheet" type="text/css" />
</head>

<body>
    <style type="text/css">
        * {
            font-size: 12px;
        }

        .page-break {
            page-break-after: always;
        }

        .text-center {
            text-align: center;
        }

        table.table-bordered {
            border: 1px solid black;
            margin-top: 20px;
        }

        table.table-bordered>thead>tr>th {
            border: 1px solid black;
        }

        table.table-bordered>tbody>tr>td {
            border: 1px solid black;
        }

        table#table-header>tbody>tr>td {
            border: none;
            padding: 0px;
            margin: 0px;
        }

        table#table-header-info>tbody>tr>td {
            border: none;
            padding: 2px;
            margin: 0px;
        }

        table#table-main>tbody>tr>td {
            padding: 10px;
            margin: 0px;
        }

        table#table-footer {
            border: none;
            margin-top: 2px;
            margin-bottom: 10px;
        }

        table#table-footer>thead>tr>th {
            border: none;
            padding: 0px;
            margin: 0px;
        }

        table#table-footer>tbody>tr>td {
            border: none;
            padding: 0px;
            margin: 0px;
        }

        hr {
            display: block;
            height: 1px;
            border: 0;
            border-top: 2px solid grey;
            margin: 0.5em 0;
            padding: 0;
        }

        /* tr td {
            padding: 0px !important;
        } */

    </style>

    <table id="table-header" class="table table-border p-0 m-0">
        <tr class="p-0 m-0">
            <td class="text-center" style="width: 15%;">
                <img class="mt-2 mb-0 pb-0" src="{{ public_path('/assets/images/logo.png') }}" height="70">
            </td>
            <td class="text-center" style="width: 70%;">
                <h2 class="m-0 p-0">{{ setting()->nama }}</h2>
                <p class="m-0 p-0">{{ setting()->alamat }}</p>
                <p class="m-0 p-0">{{ setting()->email }}</p>
                <p class="m-0 p-0">{{ setting()->telepon }}</p>
            </td>
            <td class="text-center" style="width: 15%;">
                <h2 class="m-0 p-0 text-muted">{{ $pasien->kode_harian }}</h2>
            </td>
        </tr>
    </table>
    <hr>
    <h5 class="text-center font-weight-bold text-uppercase">PEMERIKSAAN LABORTORIUM</h5>
    <p class="text-center font-weight-bold">Nomor : {{ $nomorSurat }}</p>
    <p>Yang bertanda tangan di bawah ini : </p>

    <table id="table-header-info" class="table m-0 pb-3">
        <tr>
            <td>Nama Pasien</td>
            <td>: {{ $pasien->nama }}</td>
            <td class="text-right" rowspan="5" style="width: 25%;">
                <div style="text-align:center; font-weight:bold;font-size:12px; padding:5px;">
                    <img src="data:image/png;base64, {!! $qrcode !!}">
                </div>
            </td>
        </tr>

        <tr>
            <td>NIK</td>
            <td>: {{ $pasien->nik }}</td>
        </tr>

        <tr>
            <td>Tempat, Tgl Lahir</td>
            <td>: {{ $pasien->tempat_lahir }} / {{ tanggalIndoParse($pasien->tgl_lahir) }}</td>
        </tr>
        <tr>
            <td>Jenis Kelamin</td>
            <td>: {{ $pasien->jenis_kelamin }}</td>
        </tr>
        <tr>
            <td>Pekerjaan</td>
            <td>: {{ $pasien->pekerjaan }}</td>
        </tr>
        <tr>
            <td>Alamat</td>
            <td colspan="2">: {{ $pasien->alamat }}</td>
        </tr>
    </table>
    <table id="table-main" class="table table-bordered border-dark">
        <thead class="text-center text-capitalize">
            <tr>
                <th scope="col" style="width: 15px">No</th>
                <th scope="col" style="width: 100%;">Jenis Pemeriksaan</th>
                <th scope="col" style="width: 100%;">Waktu</th>
                <th scope="col">Nilai Normal</th>
                <th scope="col">Hasil</th>
                <th scope="col" style="width: 5%;">Ket</th>
            </tr>
        </thead>
        <tbody>
            @php $no = 1; @endphp
                <tr>
                    <td scope="row" class="text-center">1</td>
                    <td class="text-capitalize">{{ $results->uraian }}</td>
                    <td class="text-capitalize">{{ $results->pukul_pemeriksaan }}&nbsp;{{ $results->tgl_pemeriksaan }}</td>
                    <td class="text-center text-capitalize">{{ $results->nilai_normal }}</td>
                    <td class="text-center text-capitalize">{{ $results->hasil }}</td>
                    <td class="text-center text-capitalize"></td>
                </tr>
        </tbody>
    </table>
    <p>Demikian surat keterangan ini dibuat sesuai dengan kondisi sebenarnya dan untuk dipergunakan sebagaimana mestinya. Hasil pemeriksaan {{ $results->uraian }} <strong>{{ $results->hasil }}</strong>  </p>
    <table id="table-footer" class="table text-center pt-2">
        <tr>
            <td style="width: 50%;">&nbsp;</td>
            <td style="width: 50%;">{{ setting()->kabupaten }}, {{ tanggalIndo() }}</td>
        </tr>
        <tr>
            <td style="width: 50%;">Dokter</td>
            <td style="width: 50%;">Analis</td>
        </tr>
        <tr>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td class="font-weight-bold">{{ $results->dokter }}</td>
            <td class="font-weight-bold">{{ $results->analis }}</td>
        </tr>
    </table>
</body>

</html>
