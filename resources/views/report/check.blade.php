<!DOCTYPE html>
<html>

<head>
    <title>{{ $queryPasien->nama}}</title>
    <meta charset="utf-8">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
</head>

<body>
    <div class="row justify-content-center">
        <div class="col-lg-11 col-md-11">
            <div class="page-title-box mt-3">
                <div class="row align-items-center">
                    {{-- <div class="col-sm-1 text-right pr-3 pl-3">
                        <img src="{{ asset('assets\images\logo.png') }}" alt="Logo" height="70" />
                    </div> --}}
                    {{-- <div class="col-sm-10 d-none d-md-block"> --}}
                    <div class="col-sm-12 text-center">
                        <h4 class="page-title font-16 mt-2 align-middle text-capitalize font-weight-bold text-primary">{{ setting()->nama }}</h4>
                        <p>{{ setting()->alamat }}<br>{{ setting()->email }}<br>{{ setting()->telepon }}</p>
                    </div>
                </div>
            </div>
            <div class="card">
                <div class="card-body">
                    <h4 class="card-title  mt-0">Hasil Pemeriksaan</h4>
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="card">
                                <div class="card-body">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <table class="table">
                                                <tbody>
                                                    <tr>
                                                        <th class="text-capitalize">No Rekam Medis</th>
                                                        <th class="text-capitalize" style="width: 20px;">:</th>
                                                        <td class="text-uppercase">{{ $queryPasien->no_rekam_medis }}</th>
                                                    </tr>
                                                    <tr>
                                                        <th class="text-capitalize">nama lengkap</th>
                                                        <th class="text-capitalize" style="width: 20px;">:</th>
                                                        <td class="text-uppercase">{{ $queryPasien->nik }}</th>
                                                    </tr>
                                                    <tr>
                                                        <th class="text-capitalize">nama lengkap</th>
                                                        <th class="text-capitalize" style="width: 20px;">:</th>
                                                        <td class="text-uppercase">{{ $queryPasien->nama }}</th>
                                                    </tr>
                                                    <tr>
                                                        <th class="text-capitalize">alamat</th>
                                                        <th class="text-capitalize" style="width: 20px;">:</th>
                                                        <td class="text-uppercase">{{ $queryPasien->alamat }}</th>
                                                    </tr>
                                                </tbody>
                                            </table>

                                            <table id="table-main" class="table table-bordered border-dark">
                                                <thead class="text-center text-capitalize">
                                                    <tr>
                                                        <th class="text-uppercase" scope="col" style="width: 15px">No</th>
                                                        <th class="text-uppercase" scope="col" style="width: 40%;">Pemeriksaan</th>
                                                        <th class="text-uppercase" scope="col">Nilai Normal</th>
                                                        <th class="text-uppercase" scope="col">Hasil</th>
                                                        <th class="text-uppercase" scope="col">Tgl Pemeriksaan</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <tr>
                                                        <td scope="row" class="text-center">1</td>
                                                        <td class="text-capitalize">{{ $queryPemeriksaan->uraian }}</td>
                                                        <td class="text-center text-capitalize">{{ $queryPemeriksaan->nilai_normal }}</td>
                                                        <td class="text-center text-capitalize font-weight-bold">{{ $queryPemeriksaan->hasil}}</td>
                                                        <td class="text-center text-capitalize font-weight-bold">{{ $queryPemeriksaan->tgl_pemeriksaan }}</td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <footer class="footer block text-center mb-2 mt-3">
                © <script type="application/javascript">
                    document.write(new Date().getFullYear())

                </script>
                <span class="d-none d-sm-inline-block  text-uppercase font-weight-bold">
                    <i class="mdi mdi-heart text-danger"></i>
                    <a href="#">IT {{ setting()->nama }}</a>
                </span>.
            </footer>
        </div>
    </div>


    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
</body>

</html>
