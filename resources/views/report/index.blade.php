@extends('layouts.global')
@section('title') Report @endsection
@section('content')
    <div class="box">
        <div class="box-header">
            <div class="row">
                <div class="col-md-6">
                    <h3 class="box-title">Report</h3>
                </div>
            </div>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
            <form class="form-horizontal" action="{{ route('report-excel.create') }}" target="_blank">
                <div class="form-group" style="margin: 5px">
                    <label for="tempat_lahir" class="col-sm-2 control-label text-uppercase">Periode Laporan : </label>
                    <div class="col-sm-10" style="">
                        <div class="form-group">
                            <div class="col-md-12">
                                <div class="row input-daterange">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <div class="col-md-12">
                                                <input type="text" name="from_date" id="from_date" class="form-control" placeholder="From Date" readonly />
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <div class="col-md-12">
                                                <input type="text" name="to_date" id="to_date" class="form-control" placeholder="To Date" readonly />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="form-group" style="margin: 5px">
                    <label for="pemeriksaan" class="col-sm-2 control-label text-uppercase">Jenis Pemeriksaan : </label>
                    <div class="col-sm-10">
                        <select class="form-control text-uppercase" id="pemeriksaan" name="pemeriksaan">
                            <option value="all">--SEMUA PEMERIKSAAN--</option>
                            @foreach($inspection as $key => $v)
                            <option value="{{ $v->id }}">{{ $v->uraian }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="form-group text-right" style="margin: 5px; margin-top: 25px">
                    <label for="btn" class="col-sm-2 control-label text-uppercase"></label>
                    <div class="col-sm-10">
                        <button type="submit" class="btn btn-primary" id="submit">Tampilkan Laporan</button>

                    </div>
                </div>
            </form>
        </div>
        <!-- /.box-body -->
    </div>
@endsection

@section('javascript')
    <script type="application/javascript">
    $('.input-daterange').datepicker({
        todayBtn: 'linked',
        format: 'yyyy-mm-dd',
        autoclose: true
    })

    </script>
@endsection
