<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;

class PatientReferral extends Model
{
    use HasFactory;

    protected $guarded = [];

    public static function generateToken()
    {
        return Str::random(10);
    }
}
