<?php

namespace App\Http\Controllers;

use App\Models\DataBase;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class DataBaseController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $result = DataBase::query();
        if (request()->ajax()) {
            return datatables()->of($result)
                ->addColumn('action', function ($data) {
                    $action = '<a href="javascript:void(0)" data-toggle="tooltip"  data-id="' . $data->id . '" class="btnEdit btn btn-primary btn-sm"><i class="fa fa-fw fa-edit"></i></a>';
                    return $action;
                })
                ->rawColumns(['action'])
                ->addIndexColumn()
                ->make(true);
        }
        return view('database.index');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, DataBase $database)
    {
        $this->validate($request, [
            'nama' => 'required',
            'email' => 'required',
            'kabupaten' => 'required',
            'telepon' => 'required',
            'alamat' => 'required',
            'logo' => 'image|mimes:png|max:2048',
        ]);

        try {
            $id = $request->get('id');
            $action = $id ? 'update' : 'create';

            $data = $request->all();

            if ($request->file('logo')) {
                if (file_exists(storage_path('app/public/assets/images' . 'logo.png'))) {
                    Storage::delete('public/assets/images/' . 'logo.png');
                }

                $imageName = 'logo.png';
                $request->logo->move(public_path('assets/images'), $imageName);
                $data['logo'] = $imageName;
            }

            if ($action == 'create') {
                $database->create($data);

            } else {
                $database::where('id', 1)
                    ->update($data);
            }

            return response()->json([
                'status' => 'success',
                'message' => 'Data has been ' . $action,
                'action' => $action,
            ], 201);
        } catch (\Exception$e) {
            return response()->json([
                'color' => 'red',
                'status' => 'error',
                'message' => $e->getMessage(),
            ], 401);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\DataBase  $database
     * @return \Illuminate\Http\Response
     */
    public function edit(DataBase $database)
    {
        return response()->json([
            'database' => $database,
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\DataBase  $database
     * @return \Illuminate\Http\Response
     */
    public function destroy(DataBase $database)
    {
        if ($database->id == 1) {
            return response()->json([
                'color' => 'red',
                'status' => 'Failed',
                'message' => 'Data cant be deleted',
            ], 201);
        }

        $database->delete();
        return response()->json([
            'status' => 'success',
            'message' => 'Data has been deleted',
        ], 201);
    }
}
