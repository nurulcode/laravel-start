<?php

namespace App\Http\Controllers\System;

use App\Http\Controllers\Controller;
use App\Http\Requests\UserRequest;
use App\Http\Resources\UserResource;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $result = User::query();
        if (request()->ajax()) {
            return datatables()->of($result)
                ->addColumn('action', function ($data) {
                    $action = '<a href="javascript:void(0)" data-toggle="tooltip"  data-id="' . $data->id . '" class="btnEdit btn btn-primary btn-sm"><i class="fa fa-fw fa-edit"></i></a>';
                    $action .= '&nbsp;';
                    $action .= '<button type="button" name="delete" id="' . $data->id . '" class="btnDelete btn btn-danger btn-sm"><i class="fa fa-fw fa-trash"></i></button>';
                    return $action;
                })
                ->rawColumns(['action'])
                ->addIndexColumn()
                ->make(true);
        }
        return view('system.user.index');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(UserRequest $request)
    {
        try {
            $id = $request->get('id');
            $action = $id ? 'update' : 'create';

            if ($action == 'create') {
                $createUser = new User();
                $createUser->name = $request->name;
                $createUser->email = $request->email;
                if ($request->password) {
                    $createUser->password = Hash::make($request->password);
                } else {
                    $createUser->password = Hash::make('password');
                }
                $createUser->save();
            } else {
                $update = User::where('id', $id)->first();
                $update->name = $request->name;
                $update->email = $request->email;
                if ($request->password) {
                    $update->password = Hash::make($request->password);
                }
                $update->save();
            }

            return response()->json([
                'status' => 'success',
                'message' => 'Data has been ' . $action,
                'action' => $action,
            ], 201);
        } catch (\Exception$e) {
            return response()->json([
                'color' => 'red',
                'status' => 'error',
                'message' => $e->getMessage(),
            ], 401);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(User $user)
    {
        return response()->json([
            'user' => new UserResource($user),
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(User $user)
    {
        if ($user->id == auth()->user()->id) {
            return response()->json([
                'color' => 'red',
                'status' => 'Failed',
                'message' => 'Data cant be deleted',
            ], 201);
        }
        $user->delete();
        return response()->json([
            'status' => 'success',
            'message' => 'Data has been deleted',
        ], 201);
    }
}
