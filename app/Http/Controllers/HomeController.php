<?php

namespace App\Http\Controllers;

use App\Models\PatientExamination;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $negative = PatientExamination::where('hasil', 'Negative')->whereDate('created_at', '=', date('Y-m-d'))->count();
        $positive = PatientExamination::where('hasil', 'Positive')->whereDate('created_at', '=', date('Y-m-d'))->count();
        $pemeriksaan = PatientExamination::whereDate('created_at', '=', date('Y-m-d'))->count();
        $date = date('d-m-Y');

        return view('home', compact('pemeriksaan', 'negative', 'positive', 'date'));
    }
}
