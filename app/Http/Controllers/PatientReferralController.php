<?php

namespace App\Http\Controllers;

use App\Models\Inspection;
use App\Models\PatientExamination;
use App\Models\PatientReferral;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class PatientReferralController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $result = DB::table('patient_referrals')
            ->join('patients', 'patients.id', '=', 'patient_referrals.patient_id')
            ->join('employees as analis', 'analis.id', '=', 'patient_referrals.analis')
            ->join('employees as dokter', 'dokter.id', '=', 'patient_referrals.dokter')
            ->select('patient_referrals.id', 'patient_referrals.tgl_rujukan', 'patient_referrals.catatan', 'patients.nama', 'patients.no_rekam_medis', 'analis.nama as analis', 'dokter.nama as dokter');

        if (request()->ajax()) {
            return datatables()->of($result)
                ->addColumn('action', function ($data) {
                    $action = '<a href="javascript:void(0)" data-toggle="tooltip"  data-id="' . $data->id . '" class="btnHasil btn btn-primary btn-sm">Hasil</a>';
                    $action .= '&nbsp;';
                    $patient = PatientExamination::where('patient_referral_id', $data->id)->first();

                    if (isset($patient)) {
                        $action .= '<a target="_blank" href="' . route('report-pdf.edit', $data->id) . '" class="btn btn-success btn-sm">Print</a>';
                    }

                    return $action;
                })
                ->rawColumns(['action'])
                ->addIndexColumn()
                ->make(true);
        }

        $inspections = Inspection::get();
        return view('patient-referral.index', compact('inspections'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, PatientReferral $patientReferral)
    {
        $this->validate($request, [
            'patient_id' => 'required',
            'tgl_rujukan' => 'required',
            'catatan' => 'required',
            'analis' => 'required',
            'dokter' => 'required',
        ]);

        try {
            $id = $request->get('id');
            $action = $id ? 'update' : 'create';

            $today = \Carbon\Carbon::now();
            setlocale(LC_ALL, 'id_ID.UTF8', 'id_ID.UTF-8', 'id_ID.8859-1', 'id_ID', 'en_US.UTF8', 'en_US.UTF-8', 'en_US');
            \Carbon\Carbon::setLocale('id');
            $hari = substr(\Carbon\Carbon::parse($today)->isoFormat('dddd'), 0, 1);
            $tanggal = \Carbon\Carbon::parse($today)->isoFormat('D');
            $bulan = substr(\Carbon\Carbon::parse($today)->isoFormat('MMMM'), 0, 1);
            $kode = $hari . '' . $tanggal . '' . $bulan;

            $patientReferral->create(array_merge($request->except('token'), [
                'token' => PatientReferral::generateToken(),
                'kode_harian' => $kode,
            ]));

            return response()->json([
                'status' => 'success',
                'message' => 'Data has been ' . $action,
                'action' => $action,
            ], 201);
        } catch (\Exception$e) {
            return response()->json([
                'color' => 'red',
                'status' => 'error',
                'message' => $e->getMessage(),
            ], 401);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\PatientReferral  $patientReferral
     * @return \Illuminate\Http\Response
     */
    public function edit(PatientReferral $patientReferral)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\PatientReferral  $patientReferral
     * @return \Illuminate\Http\Response
     */
    public function destroy(PatientReferral $patientReferral)
    {
        $patientReferral->delete();
        return response()->json([
            'status' => 'success',
            'message' => 'Data has been deleted',
        ], 201);
    }
}
