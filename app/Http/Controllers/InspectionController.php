<?php

namespace App\Http\Controllers;

use App\Models\Inspection;
use Illuminate\Http\Request;

class InspectionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $result = Inspection::query();
        if (request()->ajax()) {
            return datatables()->of($result)
                ->addColumn('action', function ($data) {
                    $action = '<a href="javascript:void(0)" data-toggle="tooltip"  data-id="' . $data->id . '" class="btnEdit btn btn-primary btn-sm"><i class="fa fa-fw fa-edit"></i></a>';
                    $action .= '&nbsp;';
                    $action .= '<button type="button" name="delete" id="' . $data->id . '" class="btnDelete btn btn-danger btn-sm"><i class="fa fa-fw fa-trash"></i></button>';
                    return $action;
                })
                ->rawColumns(['action'])
                ->addIndexColumn()
                ->make(true);
        }
        return view('inspection.index');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, Inspection $inspection)
    {
        $this->validate($request, [
            'kode' => 'required',
            'uraian' => 'required',
            'satuan' => 'required',
            'nilai_normal' => 'required',
        ]);

        try {
            $id = $request->get('id');
            $action = $id ? 'update' : 'create';

            if ($action == 'create') {
                $inspection->create($request->except('id'));

            } else {
                $inspection::where('id', $id)
                    ->update($request->except('id'));
            }

            return response()->json([
                'status' => 'success',
                'message' => 'Data has been ' . $action,
                'action' => $action,
            ], 201);
        } catch (\Exception$e) {
            return response()->json([
                'color' => 'red',
                'status' => 'error',
                'message' => $e->getMessage(),
            ], 401);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Inspection  $inspection
     * @return \Illuminate\Http\Response
     */
    public function edit(Inspection $inspection)
    {
        return response()->json([
            'inspection' => $inspection,
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Inspection  $inspection
     * @return \Illuminate\Http\Response
     */
    public function destroy(Inspection $inspection)
    {
        $inspection->delete();
        return response()->json([
            'status' => 'success',
            'message' => 'Data has been deleted',
        ], 201);
    }
}
