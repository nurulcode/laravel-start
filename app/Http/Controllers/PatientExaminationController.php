<?php

namespace App\Http\Controllers;

use App\Models\PatientExamination;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class PatientExaminationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $result = DB::table('patient_examinations')
            ->where('patient_examinations.patient_referral_id', $request->id)
            ->join('inspections', 'inspections.id', '=', 'patient_examinations.inspection_id')
            ->select('patient_examinations.id', 'patient_examinations.tgl_pemeriksaan', 'patient_examinations.catatan', 'patient_examinations.hasil', 'inspections.uraian');

        if (request()->ajax()) {
            return datatables()->of($result)
                ->addColumn('action', function ($data) {
                    $action = '<a href="javascript:void(0)" data-toggle="tooltip"  data-id="' . $data->id . '" class="btnDelete btn btn-danger btn-sm"><i class="fa fa-fw fa-trash"></i></a>';
                    return $action;
                })
                ->rawColumns(['action'])
                ->addIndexColumn()
                ->make(true);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, PatientExamination $patientExamination)
    {
        $this->validate($request, [
            'patient_referral_id' => 'required',
            'inspection_id' => 'required',
            'tgl_pemeriksaan' => 'required',
            'hasil' => 'required',
        ]);

        try {
            $id = $request->get('id');
            $action = $id ? 'update' : 'create';

            if ($action == 'create') {
                $patientExamination->create($request->except('id'));

            } else {
                $patientExamination::where('id', $id)
                    ->update($request->except('id'));
            }

            return response()->json([
                'status' => 'success',
                'message' => 'Data has been ' . $action,
                'action' => $action,
            ], 201);
        } catch (\Exception$e) {
            return response()->json([
                'color' => 'red',
                'status' => 'error',
                'message' => $e->getMessage(),
            ], 401);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\PatientExamination  $patientExamination
     * @return \Illuminate\Http\Response
     */
    public function show(PatientExamination $patientExamination)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\PatientExamination  $patientExamination
     * @return \Illuminate\Http\Response
     */
    public function edit(PatientExamination $patientExamination)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\PatientExamination  $patientExamination
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, PatientExamination $patientExamination)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\PatientExamination  $patientExamination
     * @return \Illuminate\Http\Response
     */
    public function destroy(PatientExamination $patientExamination)
    {
        $patientExamination->delete();
        return response()->json([
            'status' => 'success',
            'message' => 'Data has been deleted',
        ], 201);
    }
}
