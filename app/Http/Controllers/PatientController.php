<?php

namespace App\Http\Controllers;

use App\Models\Employee;
use App\Models\Patient;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class PatientController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $result = Patient::query();
        if (request()->ajax()) {
            return datatables()->of($result)
                ->addColumn('action', function ($data) {
                    $action = '<a href="javascript:void(0)" data-toggle="tooltip"  data-id="' . $data->id . '" class="btnEdit btn btn-primary btn-sm">Edit</i></a>';
                    $action .= '&nbsp;';
                    $action .= '<a href="javascript:void(0)" data-toggle="tooltip"  data-id="' . $data->id . '" data-no_rekam_medis="' . $data->no_rekam_medis . '" class="btnRujuk btn btn-success btn-sm">Kirim</a>';

                    return $action;
                })
                ->rawColumns(['action'])
                ->addIndexColumn()
                ->make(true);
        }

        $employees = Employee::get();
        return view('patient.index', compact('employees'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, Patient $patient)
    {
        $this->validate($request, [
            'nama' => 'required',
            'nik' => 'required',
            'tempat_lahir' => 'required',
            'tgl_lahir' => 'required',
            'agama' => 'required',
            'telepon' => 'required',
            'alamat' => 'required',
            'jenis_kelamin' => 'required',
            'pekerjaan' => 'required',
        ]);

        try {
            $id = $request->get('id');
            $action = $id ? 'update' : 'create';

            $last = $patient->orderby('no_rekam_medis', 'desc')->first();

            if (isset($last->no_rekam_medis)) {
                $RM = $last->no_rekam_medis;
            } else {
                $RM = 00000;
            }

            $rekam_medis = str_pad($RM + 1, 6, 0, STR_PAD_LEFT);

            if ($action == 'create') {
                $patient->create(array_merge($request->except('id', 'no_rekam_medis'), [
                    'no_rekam_medis' => $rekam_medis,
                ]));
            } else {
                $patient::where('id', $id)
                    ->update($request->except('id', 'no_rekam_medis'));
            }

            return response()->json([
                'status' => 'success',
                'message' => 'Data has been ' . $action,
                'action' => $action,
            ], 201);
        } catch (\Exception$e) {
            return response()->json([
                'color' => 'red',
                'status' => 'error',
                'message' => $e->getMessage(),
            ], 401);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Patient  $patient
     * @return \Illuminate\Http\Response
     */
    public function edit(Patient $patient)
    {
        return response()->json([
            'patient' => $patient,
        ]);
    }

    public function check(Request $request)
    {
        $queryPasien = DB::table('patients as a')
            ->join('patient_referrals as b', 'b.patient_id', '=', 'a.id')
            ->where('b.token', '=', $request->key)
            ->select('a.id', 'a.*', 'b.token', 'b.kode_harian', 'b.id as patient_referral_id')
            ->first();

        $queryPemeriksaan = DB::table('patient_examinations as a')
            ->join('inspections as e', 'a.inspection_id', '=', 'e.id')
            ->where('a.patient_referral_id', '=', $queryPasien->patient_referral_id)
            ->select('a.id', 'a.hasil', 'a.tgl_pemeriksaan', 'a.pukul_pemeriksaan', 'e.uraian', 'e.satuan', 'e.nilai_normal')
            ->first();

        if (!isset($queryPasien)) {
            return abort(404, 'Administrator does not exist.');
        }

        return view('report.check', compact('queryPasien', 'queryPemeriksaan'));
    }
}
