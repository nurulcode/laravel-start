<?php

namespace App\Http\Controllers;

use App\Exports\ReportExport;
use App\Models\DataBase;
use App\Models\Inspection;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;

class ReportController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $inspection = Inspection::get();
        return view('report.index', compact('inspection'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $dt = Carbon::now();
        $data = DataBase::first();

        if (!$request->from_date || !$request->to_date) {
            $request->from_date = $dt->toDateString();
            $request->to_date = $dt->toDateString();
        }

        return Excel::download(
            new ReportExport(
                $request->from_date,
                $request->to_date,
                $request->pemeriksaan,
                $data
            ),
            'Laporan.xlsx'
        );
    }
}
