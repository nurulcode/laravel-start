<?php

namespace App\Http\Controllers;

use Barryvdh\DomPDF\Facade as PDF;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use SimpleSoftwareIO\QrCode\Facades\QrCode;

class ReportPdfController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $queryPasien = DB::table('patients as a')
            ->join('patient_referrals as b', 'b.patient_id', '=', 'a.id')
            ->join('patient_examinations as c', 'c.patient_referral_id', '=', 'b.id')
            ->join('employees as analis', 'b.analis', '=', 'analis.id')
            ->join('employees as dokter', 'b.dokter', '=', 'dokter.id')
            ->join('inspections as e', 'c.inspection_id', '=', 'e.id')
            ->where('c.patient_referral_id', '=', $request->patient_referral_id)
            ->select('a.id', 'a.*', 'b.token', 'b.kode_harian', 'b.id as nomor')
            ->first();

        $queryPemeriksaan = DB::table('patients as a')
            ->join('patient_referrals as b', 'b.patient_id', '=', 'a.id')
            ->join('patient_examinations as c', 'c.patient_referral_id', '=', 'b.id')
            ->join('employees as analis', 'b.analis', '=', 'analis.id')
            ->join('employees as dokter', 'b.dokter', '=', 'dokter.id')
            ->join('inspections as e', 'c.inspection_id', '=', 'e.id')
            ->where('c.patient_referral_id', '=', $request->patient_referral_id)
            ->select('c.id', 'analis.nama as analis', 'dokter.nama as dokter', 'c.tgl_pemeriksaan', 'c.pukul_pemeriksaan', 'c.hasil', 'c.catatan', 'e.uraian', 'b.kode_harian', 'e.nilai_normal', 'e.satuan')
            ->first();

        $url = env('URL_APP') . '/laboratorium/pasien?key=' . $queryPasien->token;
        $qrcode = base64_encode(QrCode::format('svg')->size(100)->errorCorrection('H')->generate($url));

        $pdf = PDF::loadview('report.pdf', [
            'pasien' => $queryPasien,
            'results' => $queryPemeriksaan,
            'qrcode' => $qrcode,
        ])->setPaper(array(0, 0, 609.4488, 935.433), 'potrait');

        return $pdf->stream('Hasil Pemeriksaan Pcr ' . $queryPasien->nama . '.pdf');

        return view('report.pdf');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $queryPasien = DB::table('patients as a')
            ->join('patient_referrals as b', 'b.patient_id', '=', 'a.id')
            ->where('b.id', '=', $id)
            ->select('a.id', 'a.*', 'b.token', 'b.kode_harian', 'b.id as nomor')
            ->first();

        $queryPemeriksaan = DB::table('patients as a')
            ->join('patient_referrals as b', 'b.patient_id', '=', 'a.id')
            ->join('patient_examinations as c', 'c.patient_referral_id', '=', 'b.id')
            ->join('employees as analis', 'b.analis', '=', 'analis.id')
            ->join('employees as dokter', 'b.dokter', '=', 'dokter.id')
            ->join('inspections as e', 'c.inspection_id', '=', 'e.id')
            ->where('c.patient_referral_id', '=', $id)
            ->select('c.id', 'analis.nama as analis', 'dokter.nama as dokter', 'c.tgl_pemeriksaan', 'c.pukul_pemeriksaan', 'c.hasil', 'c.catatan', 'e.uraian', 'b.kode_harian', 'e.nilai_normal', 'e.satuan')
            ->first();

        $url = env('URL_APP') . '/patient/check?key=' . $queryPasien->token;
        $qrcode = base64_encode(QrCode::format('svg')->size(100)->errorCorrection('H')->generate($url));

        $today = \Carbon\Carbon::now();
        $tahun = \Carbon\Carbon::parse($today)->isoFormat('Y');
        $nomorSurat = str_pad($queryPasien->nomor + 1, 3, 0, STR_PAD_LEFT) . ' / KP-PM / ' . getRomawi() . ' / ' . $tahun;

        $pdf = PDF::loadview('report.pdf', [
            'pasien' => $queryPasien,
            'results' => $queryPemeriksaan,
            'qrcode' => $qrcode,
            'nomorSurat' => $nomorSurat,
        ])->setPaper(array(0, 0, 609.4488, 935.433), 'potrait');

        return $pdf->stream('Hasil Pemeriksaan Pcr ' . $queryPasien->nama . '.pdf');

        return view('report.pdf');
    }
}
