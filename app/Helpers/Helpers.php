<?php
use App\Models\DataBase;

function setting()
{
    return DataBase::first();
}

function tanggalIndo()
{
    setlocale(LC_ALL, 'id_ID.UTF8', 'id_ID.UTF-8', 'id_ID.8859-1', 'id_ID', 'en_US.UTF8', 'en_US.UTF-8', 'en_US');
    \Carbon\Carbon::setLocale('id');
    return \Carbon\Carbon::now()->isoFormat('D MMMM Y');
}

function tanggalIndoParse($date)
{
    setlocale(LC_ALL, 'id_ID.UTF8', 'id_ID.UTF-8', 'id_ID.8859-1', 'id_ID', 'en_US.UTF8', 'en_US.UTF-8', 'en_US');
    \Carbon\Carbon::setLocale('id');
    return \Carbon\Carbon::parse($date)->isoFormat('D MMMM Y');
}

function getRomawi()
{
    $today = \Carbon\Carbon::now();
    $bln = \Carbon\Carbon::parse($today)->isoFormat('M');

    switch ($bln) {
        case 1:
            return "I";
            break;
        case 2:
            return "II";
            break;
        case 3:
            return "III";
            break;
        case 4:
            return "IV";
            break;
        case 5:
            return "V";
            break;
        case 6:
            return "VI";
            break;
        case 7:
            return "VII";
            break;
        case 8:
            return "VIII";
            break;
        case 9:
            return "IX";
            break;
        case 10:
            return "X";
            break;
        case 11:
            return "XI";
            break;
        case 12:
            return "XII";
            break;
    }
}
