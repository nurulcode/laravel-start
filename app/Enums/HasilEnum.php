<?php

namespace App\Enums;

use BenSampo\Enum\Enum;

/**
 * @method static static OptionOne()
 * @method static static OptionTwo()
 * @method static static OptionThree()
 */
final class HasilEnum extends Enum
{
    const NEGATIVE = 1;
    const POSITIVE = 2;
}
