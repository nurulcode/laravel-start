<?php

namespace App\Enums;

use BenSampo\Enum\Enum;

/**
 * @method static static OptionOne()
 * @method static static OptionTwo()
 * @method static static OptionThree()
 */
final class AgamaEnum extends Enum
{
    const ISLAM = 1; //(int) 1 or (string) Islam ? isl
    const PROTESTAN = 2;
    const KATOLIK = 3;
    const HINDU = 4;
    const BUDDHA = 5;
    const KHONGHUCU = 6;
    const LAIN = 7;
}
