<?php

namespace App\Exports;

use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Events\AfterSheet;

class ReportExport implements ShouldAutoSize, WithEvents
{
    private $judul = 'Laporan Pemeriksaan';

    public function __construct($from_date, $to_date, $pemeriksaan, $data)
    {
        $this->from_date = $from_date;
        $this->to_date = $to_date;
        $this->pemeriksaan = $pemeriksaan;
        $this->data = $data;
    }

    public function registerEvents(): array
    {
        return [
            AfterSheet::class => function (AfterSheet $event) {
                $cell = 'O';
                $event->sheet->getStyle('A1')->getAlignment()->setHorizontal('center');
                $event->sheet->mergeCells('A1:' . $cell . '1')->getCell('A1')->setValue($this->judul);
                // set headers
                $event->sheet->styleCells(
                    'A1:A2',
                    [
                        'font' => [
                            'bold' => true,
                        ],
                    ]
                );

                $event->sheet->styleCells(
                    'A2:' . $cell . '2',
                    [
                        'alignment' => [
                            'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
                        ],
                        'font' => [
                            'bold' => true,
                        ],
                    ]
                );

                $event->sheet->styleCells(
                    'A3:' . $cell . '3',
                    [
                        'alignment' => [
                            'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
                        ],
                        'fill' => [
                            'fillType' => \PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID,
                            'color' => ['argb' => 'd6d4d0'],
                        ],
                    ]
                );

                $event->sheet->getCell('A3')->setValue('TGL PEMERIKSAAN');
                $event->sheet->getCell('B3')->setValue('PUKUL PEMERIKSAAN');
                $event->sheet->getCell('C3')->setValue('NOMOR');
                $event->sheet->getCell('D3')->setValue('NAMA');
                $event->sheet->getCell('E3')->setValue('NIK');
                $event->sheet->getCell('F3')->setValue('TEMPAT LAHIR');
                $event->sheet->getCell('G3')->setValue('TGL LAHIR');
                $event->sheet->getCell('H3')->setValue('JK');
                $event->sheet->getCell('I3')->setValue('PEKERJAAN');
                $event->sheet->getCell('J3')->setValue('ALAMAT');
                $event->sheet->getCell('K3')->setValue('TELEPON');
                $event->sheet->getCell('L3')->setValue('ANALIS');
                $event->sheet->getCell('M3')->setValue('DOKTER');
                $event->sheet->getCell('N3')->setValue('PEMERIKSAAN');
                $event->sheet->getCell('O3')->setValue('URAIAN');

                $event->sheet->getCell('A4')->setValue('1');
                $event->sheet->getCell('B4')->setValue('2');
                $event->sheet->getCell('C4')->setValue('3');
                $event->sheet->getCell('D4')->setValue('4');
                $event->sheet->getCell('E4')->setValue('5');
                $event->sheet->getCell('F4')->setValue('6');
                $event->sheet->getCell('G4')->setValue('7');
                $event->sheet->getCell('H4')->setValue('8');
                $event->sheet->getCell('I4')->setValue('9');
                $event->sheet->getCell('J4')->setValue('10');
                $event->sheet->getCell('K4')->setValue('11');
                $event->sheet->getCell('L4')->setValue('12');
                $event->sheet->getCell('M4')->setValue('13');
                $event->sheet->getCell('N4')->setValue('14');
                $event->sheet->getCell('O4')->setValue('15');

                // query
                $row = 5;
                $query = DB::table('patients as a')
                    ->join('patient_referrals as b', 'b.patient_id', '=', 'a.id')
                    ->join('patient_examinations as c', 'c.patient_referral_id', '=', 'b.id')
                    ->join('employees as analis', 'b.analis', '=', 'analis.id')
                    ->join('employees as dokter', 'b.dokter', '=', 'dokter.id')
                    ->join('inspections as e', 'c.inspection_id', '=', 'e.id')
                    ->whereBetween('c.tgl_pemeriksaan', array($this->from_date, $this->to_date));

                if ($this->pemeriksaan != 'all') {
                    $query = $query->where('c.inspection_id', '=', $this->pemeriksaan);
                }

                $query = $query->select('c.id', 'a.*', 'analis.nama as analis', 'dokter.nama as dokter', 'c.tgl_pemeriksaan', 'c.pukul_pemeriksaan', 'c.hasil', 'c.catatan', 'e.uraian', 'b.id as nomor')
                    ->get();

                foreach ($query as $v) {

                    $event->sheet->getCell('A' . $row)->setValue($v->tgl_pemeriksaan);
                    $event->sheet->getCell('B' . $row)->setValue($v->pukul_pemeriksaan);
                    $event->sheet->getCell('C' . $row)->setValue($v->nomor);
                    $event->sheet->getCell('D' . $row)->setValue($v->nama);
                    $event->sheet->getCell('E' . $row)->setValue("'" . $v->nik);
                    $event->sheet->getCell('F' . $row)->setValue($v->tempat_lahir);
                    $event->sheet->getCell('G' . $row)->setValue($v->tgl_lahir);
                    $event->sheet->getCell('H' . $row)->setValue($v->jenis_kelamin);
                    $event->sheet->getCell('I' . $row)->setValue($v->pekerjaan);
                    $event->sheet->getCell('J' . $row)->setValue($v->alamat);
                    $event->sheet->getCell('K' . $row)->setValue($v->telepon);
                    $event->sheet->getCell('L' . $row)->setValue($v->analis);
                    $event->sheet->getCell('M' . $row)->setValue($v->dokter);
                    $event->sheet->getCell('N' . $row)->setValue($v->uraian);
                    $event->sheet->getCell('O' . $row)->setValue($v->hasil);

                    $event->sheet->getStyle('A3:' . $cell . '' . $row)->applyFromArray([
                        'borders' => [
                            'allBorders' => [
                                'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                                'color' => ['argb' => '000000'],
                            ],
                        ],
                    ]);

                    $row++;
                }

            },
        ];
    }

}
