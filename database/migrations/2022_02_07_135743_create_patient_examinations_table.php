<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePatientExaminationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('patient_examinations', function (Blueprint $table) {
            $table->id();
            $table->foreignId('patient_referral_id')->constrained('patient_referrals')->onUpdate('cascade')->onDelete('cascade');
            $table->foreignId('inspection_id')->constrained('inspections')->onUpdate('cascade')->onDelete('cascade');
            $table->date('tgl_pemeriksaan');
            $table->time('pukul_pemeriksaan')->nullable()->default('000000');
            $table->string('hasil')->nullable();
            $table->string('catatan')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('patient_examinations');
    }
}
