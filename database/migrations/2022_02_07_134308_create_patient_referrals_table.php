<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePatientReferralsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('patient_referrals', function (Blueprint $table) {
            $table->id();
            $table->foreignId('patient_id')->constrained('patients')->onUpdate('cascade')->onDelete('cascade');
            $table->string('kode_harian');
            $table->date('tgl_rujukan');
            $table->string('catatan');
            $table->text('token');
            $table->foreignId('analis')->nullable()->constrained('employees')->onUpdate('cascade')->onDelete('set null');
            $table->foreignId('dokter')->nullable()->constrained('employees')->onUpdate('cascade')->onDelete('set null');
            $table->timestamps();

            // $table->unique(['patient_id', 'tgl_rujukan']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('patient_referrals');
    }
}
