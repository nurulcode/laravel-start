<?php

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
 */

Route::get('/', function () {
    return redirect('/login');
});

Route::get('/patient/check', [\App\Http\Controllers\PatientController::class, 'check'])->name('patient.check');

Auth::routes([
    'register' => false, // Registration Routes...
    'reset' => false, // Password Reset Routes...
    'verify' => false, // Email Verification Routes...
]);

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
Route::resource('user', \App\Http\Controllers\System\UserController::class);
Route::resource('patient', \App\Http\Controllers\PatientController::class);
Route::resource('inspection', \App\Http\Controllers\InspectionController::class);
Route::resource('employee', \App\Http\Controllers\EmployeeController::class);
Route::resource('database', \App\Http\Controllers\DataBaseController::class);
Route::resource('patient-referral', \App\Http\Controllers\PatientReferralController::class);
Route::resource('patient-examination', \App\Http\Controllers\PatientExaminationController::class);
Route::resource('report-excel', \App\Http\Controllers\ReportController::class)->only(['index', 'create']);
Route::resource('report-pdf', \App\Http\Controllers\ReportPdfController::class)->only(['index', 'edit']);
